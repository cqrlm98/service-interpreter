package org.epam.serviceinterpreter.exception;

public class InterpreterLoadProgramException extends RuntimeException {
    public InterpreterLoadProgramException(String msg){
        super(msg);
    }
}
