package org.epam.serviceinterpreter.service;

import org.epam.serviceinterpreter.exception.InterpreterLangUnknownException;
import org.springframework.stereotype.Component;

@Component
public class InterpreterFactory{
    public Interpreter createInterpreter(String name){
        Interpreter interpreter=null;
        String nameLowerCase = name.toLowerCase();

        switch (nameLowerCase){
            case "brainfuck":
//                return new BrainfuckInterpreter();
            case "cow":
//                return new CowInterpreter();
            case "ook":
//                return new OokInterpreter();
            case "spoon":
//                return new SpoonInterpreter();
            default:
                throw new InterpreterLangUnknownException(name);
        }
    }
}
